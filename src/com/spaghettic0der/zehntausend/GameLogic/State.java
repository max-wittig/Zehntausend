package com.spaghettic0der.zehntausend.GameLogic;


/**
 * state for validCheck in the Game Class
 */
public enum State
{
    WIN,
    ROLL,
    NEXT
}
